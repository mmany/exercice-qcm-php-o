<?php

class Reponse
{
    const BONNE_REPONSE = 1;
    private $reponse;
    public $correct;
    public $goodanswer;
    
     public function __construct($reponse, $correct="wrong") {
        $this->reponse = $reponse;
        if ($correct === self::BONNE_REPONSE)
        {
            $this->correct = 1;
            $this->goodanswer = $reponse;
        }
        else
            $this->correct = 0;
    }

    public function getChoix() {
        return $this->reponse;
    }

    public function getGoodAnswer() {
        return $this->goodanswer;
    }
}

?>