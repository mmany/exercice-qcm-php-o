<?php

class Qcm{

    public $question;
    public $appreciation;
    public $note;
    private $i;
    public $result;
    public $nombrequestion;

	public function __construct() {
        $this->question = array();
        $this->note = array();
    }

    public function ajouterQuestion($question){
        $this->question[]= $question;
        return $this;
    }

    public function setAppreciation($appreciation)
    {
        foreach ($appreciation as $index=>$text)
        {
            $this->note = explode('-', $index);
            /* var_dump($this->note); */
           
            $this->i= $this->note[0];
            while($this->i <= $this->note[1]){
                $this->i++;
                $this->appreciation[$this->i] = $text;
            }

        }
       /*  var_dump($this->appreciation);
        print "<br />"; */
    }

    public function generer()
    {
        if (isset($_POST['myqcm_submit']))
        {
            $this->result = 0;
            $this->nombrequestion = 0;
            print "You did it !";
            print "<br /><br />";
            foreach($this->question as $i => $enonce)
            {
                if (!isset($_POST['question'.$i]))
                {
                    header("Location: ../Retry/questionnaire.php");
                }
                print("<h1 style=\"color:white\">Question : ".$enonce->getQuestion()." </h1>=> ");
                //print "<br />";
                foreach($enonce->getReponse() as $x => $reponse)
                {
                    if($reponse->getGoodAnswer())
                    {
                        print($reponse->getGoodAnswer());
                        $good = $x;
                        print "<br />";
                    }
                }
                print "<br />";
                print($enonce->getExplication());
                print "<br /><br />";
                if (isset($_POST['question'.$i]))
                {
                    $checked = $_POST['question'.$i];
                    print "<br />";
                    if ($checked == $good)
                    {
                        print("<h2 style=\"color:green\">Bien joué</h2>");
                        $this->result++;
                        $this->nombrequestion++;
                        print "<br />";
                    }
                    else
                    {
                        print("<h2 style=\"color:red\">Loupé</h2>");
                        $this->nombrequestion++;
                        print "<br />";
                    }
                }
            }
            $notation = 20 / $this->nombrequestion;
            $this->result = $notation * $this->result;
            print("Resultat : ".$this->result."/20");
            print "<br /><br /><br />";
            if ($this->result == 0)
                print($this->appreciation[1]);
            else
                print($this->appreciation[$this->result]);
            print "<br /><br />";
            print("<a href=\"questionnaire.php\" class=\"btn\">Yay encore un !</a>");
        }
        else
        {
        print("<form method=\"post\" action=\"#\">");
        print("<input name=\"myqcm\" type=\"hidden\" value=\"1\">");
        foreach($this->question as $i => $enonce)
        {
            print("<h1 style=\"color:white\">Question : ".$enonce->getQuestion()."</h1>");
            print "<br />";
            foreach($enonce->getReponse() as $x => $reponse)
            {
                print('<input type="radio" id="question'.$i.'" name="question'.$i.'" value="'.$x.'"><label for="'.$x.'">'.$reponse->getChoix().'</label><br />');
            }
            print "<br /><br />";
        }
        print("<br /><input type=\"submit\" name=\"myqcm_submit\" value=\"Voir mon resultat\">");
        print("</form>");
    }
    }
}

?>