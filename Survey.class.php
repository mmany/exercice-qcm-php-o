<?php

class Question extends Qcm
{
    public $answer;
    public $quest;
    public $numquestion;
    public $explain;

    public function __construct($quest) {
        $this->numquestion = $quest;
        print("<br/>Question : ".$this->numquestion. "<br/>");
        
        //return($this->numquestion);
    }

    public function getQuest() {
        return($this->numquestion);
    }

     public function ajouterReponse($intitule)
    {
        $this->answer = $intitule;
        return $this;
    }

    public function getReponse() {
        return $this->answer;
     }

    public function setExplications($intitule)
    {
        //print($intitule."<br />");
        $this->explain = $intitule;
        return $this;
    }
   
     public function getExplication() {
        return $this->explain;
     }

}

class Reponse extends Question
{
    const BONNE_REPONSE = 1;
    
     public function __construct($data=array()) {
        $this->data = $data;
        print('<input class="buttoncss" type="radio" id="'.$data.'" name="'.$this->getQuest().'" value="'.$data.'"><label for="'.$data.'">'.$data.'</label><br />');
    }
}

?>